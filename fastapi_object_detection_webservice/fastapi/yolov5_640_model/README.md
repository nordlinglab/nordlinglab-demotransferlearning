# YOLO model download link

- [YOLOv1m](https://drive.google.com/file/d/1Q9ufc28vC6zERUHsYr7WB0TrU0Ztuj0N/view?usp=sharing)

- [YOLOv5m/Yolov5_640_model](https://drive.google.com/file/d/11PzQrl2B9MU9aKQQ7RjD8QhJFDRRDgNX/view?usp=sharing)

- [YOLOv5m6/Yolov5_1280_model](https://drive.google.com/file/d/1ferTtdNVWddmP3ltpuPq_STSSolSrHfx/view?usp=sharing)
