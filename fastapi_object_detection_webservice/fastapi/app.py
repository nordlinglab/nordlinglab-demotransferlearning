import os
import datetime
import time
import model
import numpy as np
import cv2
import aiohttp
import aiofiles
import shutil
import pathlib

from urllib.parse import quote
from apscheduler.schedulers.background import BackgroundScheduler
from fastapi import FastAPI, Request, UploadFile, File, Form
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse, RedirectResponse
from starlette.exceptions import HTTPException as StarletteHTTPException

# Initialization
THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
IMG_EXT = ['png', 'jpg', 'jpeg', 'JPG', 'PNG', 'JPEG']
VIDEO_EXT = ['mp4', 'avi', 'mov', 'mpeg', 'flv', 'mkv', 'MKV', 'wmv', 'MP4', 'AVI', 'MOV', 'MPEG', 'FLV', 'WMV']

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

scheduler = BackgroundScheduler()

async def intervaltask():
    for folder in os.path.join(THIS_FOLDER, 'static', 'dummy_images'):
        fname = pathlib.Path(os.path.join(THIS_FOLDER, 'static', 'dummy_images', folder))
        ctime = datetime.datetime.fromtimestamp(fname.stat().st_ctime)
        timenow = datetime.datetime.now()
        time_difference = (timenow - ctime).total_seconds()
        if time_difference > 600:
            shutil.rmtree(fname)
    for video in os.path.join(THIS_FOLDER, 'static', 'uploaded_video'):
        fname = pathlib.Path(os.path.join(THIS_FOLDER, 'static', 'uploaded_video', video))
        ctime = datetime.datetime.fromtimestamp(fname.stat().st_ctime)
        timenow = datetime.datetime.now()
        time_difference = (timenow - ctime).total_seconds()
        if time_difference > 600:
            os.remove(fname)

scheduler.add_job(intervaltask, 'interval', hours=2)
scheduler.start()

client_session = aiohttp.ClientSession() # Start client session

with open('counter.txt', 'r') as infile:
    data = infile.readline().split(' ')
    app.predict_img_counts = int(data[0])
    app.predict_video_counts = int(data[1])

async def scheduledtask(*args):
    if os.path.exists(args[0]+'/video.mp4'):
        os.remove(args[0]+'/video.mp4')
        os.rmdir(args[0])

@app.on_event("shutdown")
async def cleanup():
    await client_session.close()

@app.exception_handler(StarletteHTTPException)
async def custom_http_exception_handler(request, exc):
    return RedirectResponse(url='/')

@app.get('/', response_class=HTMLResponse)
async def main(request: Request):
    return templates.TemplateResponse("main.html", {"request": request, "predict_video_counts": app.predict_video_counts, "predict_img_counts": app.predict_img_counts})

@app.post('/result/', response_class=HTMLResponse)
async def predict_image(request: Request, file: UploadFile  = File(...), model_id: str = Form(...)):
    if not file:
        return RedirectResponse(url='/')
    elif file.filename == '':
        return RedirectResponse(url='/')
    else:
        if not model_id:
            model_id = '5' # default
            
        filename = file.filename
        ext = filename.split('.')[-1]

        if file and ext in IMG_EXT:
            # Process image
            start_time: float = time.time()
            contents = await file.read()
            nparr = np.fromstring(contents, np.uint8)
            img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
            img_h, img_w, _ = img.shape

            # Make prediction
            if model_id == '1':
                result, img = await model.output_predicted_image(img, client_session)
                byte_im = await model.encode_image(img)
                if any(result): 
                    has_result = True
                else:
                    has_result = False

                # Pass predicted image in data uri
                data_url = 'data:image/jpg;base64,{}'.format(quote(byte_im))

                app.predict_img_counts += 1
                with open('counter.txt', 'w') as infile:
                    infile.write(f"{app.predict_img_counts} {app.predict_video_counts}")

                # Render html for displaying the result
                print("Got predicted data in ---" + str(time.time() - start_time) + "seconds ---")
                return templates.TemplateResponse("prediction.html", {"request":request, "data_url":data_url, "IMAGE_W":img_w, "IMAGE_H":img_h, "result":result, \
                    "predict_img_counts":app.predict_img_counts, "predict_video_counts":app.predict_video_counts, "has_result":has_result})
            elif model_id == '5':
                result, img = await model.output_predicted_image_v5(img, client_session)
                byte_im = await model.encode_image(img)
                if any(result): 
                    has_result = True
                else:
                    has_result = False

                # Pass predicted image in data uri
                data_url = 'data:image/jpg;base64,{}'.format(quote(byte_im))

                app.predict_img_counts += 1
                with open('counter.txt', 'w') as infile:
                    infile.write(f"{app.predict_img_counts} {app.predict_video_counts}")
                # Render html for displaying the result
                print("Got predicted data in ---" + str(time.time() - start_time) + "seconds ---")
                return templates.TemplateResponse("predictionv5.html", {"request":request, "data_url":data_url, "IMAGE_W":img_w, "IMAGE_H":img_h, "result":result, \
                    "predict_img_counts":app.predict_img_counts, "predict_video_counts":app.predict_video_counts, "has_result":has_result})   
        elif file and ext in VIDEO_EXT:
            start_time: float = time.time()
            time_name = time.strftime("%Y%m%d-%H%M%S")
            dummy_save_folder = os.path.join(THIS_FOLDER, 'static', 'dummy_images', time_name)
            path_to_video = os.path.join(THIS_FOLDER, 'static', 'uploaded_video', filename)

            # Check if the directory exists, and make directory if dont exists
            if not os.path.exists(THIS_FOLDER + '/static/dummy_images'):
                os.makedirs(THIS_FOLDER + '/static/dummy_images')
            if not os.path.exists(THIS_FOLDER + '/static/uploaded_video'):
                os.makedirs(THIS_FOLDER + '/static/uploaded_video')
            os.makedirs(dummy_save_folder)

            # Save video
            async with aiofiles.open(path_to_video, 'wb') as out_file:
                while content := await file.read(1024):  # async read chunk
                    await out_file.write(content)  # async write chunk
            
            # Predict video frames
            vid_w, vid_h = await model.predict_video_frames(path_to_video, dummy_save_folder, model_id, client_session)

            # Create video from processed frames, first create .avi then convert to .mp4
            await model.create_prediction_video(dummy_save_folder, vid_w, vid_h)

            # Delete .jpg files in the folder
            files_in_directory = os.listdir(dummy_save_folder)
            for item in files_in_directory:
                if item.endswith(".jpg"):
                    os.remove(os.path.join(dummy_save_folder, item))

            # Schedule to delete .mp4 video in 10 minutes
            job_time = datetime.datetime.now() + datetime.timedelta(seconds=600)
            scheduler.add_job(func=scheduledtask, args=[dummy_save_folder], trigger='date', run_date=job_time, id=time_name)

            app.predict_video_counts += 1
            with open('counter.txt', 'w') as infile:
                infile.write(f"{app.predict_img_counts} {app.predict_video_counts}")
                
            print("Got predicted data in ---" + str(time.time() - start_time) + "seconds ---")
            return templates.TemplateResponse("prediction_video.html", {"request":request, "IMAGE_W":vid_w, "IMAGE_H":vid_h, "video_path":'dummy_images/'+time_name+'/video.mp4', \
                "predict_video_counts":app.predict_video_counts, "predict_img_counts":app.predict_img_counts})              
        else:
            return RedirectResponse(url='/')
