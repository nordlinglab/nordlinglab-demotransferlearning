# Things you should know

1. Use virtual environment to install all required python modules to avoid dependencies problems with your computer. Learn more about virtual environment for [Ubuntu OS](https://www.geeksforgeeks.org/python-virtual-environment/) or [Windows OS](https://docs.python.org/3/library/venv.html#creating-virtual-environments).
1. Learn more about [Asynchronous/concurrency vs synchronous](https://fastapi.tiangolo.com/async/#asynchronous-code). FastAPI is an asynchronous web framework.
1. Learn how to use [Bootstrap](https://getbootstrap.com/) for HTML design.

# Introduction

1. [FastAPI](https://fastapi.tiangolo.com/)

   FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.6+ based on standard Python type hints.

   The key features are:

   - Fast: Very high performance, on par with NodeJS and Go (thanks to Starlette and Pydantic). [One of the fastest Python frameworks available](https://fastapi.tiangolo.com/#performance).
   - Fast to code: Increase the speed to develop features by about 200% to 300%. \*
   - Fewer bugs: Reduce about 40% of human (developer) induced errors. \*
   - Intuitive: Great editor support. Completion everywhere. Less time debugging.
   - Easy: Designed to be easy to use and learn. Less time reading docs.
   - Short: Minimize code duplication. Multiple features from each parameter declaration. Fewer bugs.
   - Robust: Get production-ready code. With automatic interactive documentation.
   - Standards-based: Based on (and fully compatible with) the open standards for APIs: [OpenAPI](https://github.com/OAI/OpenAPI-Specification) (previously known as Swagger) and JSON Schema

1. [Uvicorn](https://www.uvicorn.org/)

   Uvicorn is a lightning-fast ASGI server implementation

1. [Docker](https://www.docker.com/)

   Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.

1. [NGINX](https://www.nginx.com/)

   NGINX accelerates content and application delivery, improves security, facilitates availability and scalability for the busiest web sites on the Internet.

## FastAPI and Uvicorn

Run FastAPI application in development server:

        uvicorn app:app --reload

where the 'first' app refers to python file named 'app' and the 'second' app refers to 'app = FastApi()' in the python file.

By enabling option --reload, the server will restart automatically if it detects any change in the code.

Other useful options:

1. --port, set specific port

   Usage example: --port=8888

1. --host, set specific host

   Usage example: --host='0.0.0.0'

# Installation pre-requisites

- Nvidia driver and CUDA
- [docker and nvidia-docker 2](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#installing-on-ubuntu-and-debian)

# How to run the service

Run this in 'fastapi_object_detection_webservice/' folder

        docker-compose up

# NGINX configurations

## Client > NGINX (port 80) > Docker (port 90)

File location: /etc/nginx/nginx.conf

      server {
         listen 80;
         send_timeout 6000;
         proxy_connect_timeout 6000;
         proxy_send_timeout 6000;
         proxy_read_timeout 6000;
         client_body_timeout 6000;
         client_max_body_size 20M;
         client_body_buffer_size 50M;

         server_name where.nordlinglab.org;

         location / {
            proxy_set_header Host $http_host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
            proxy_redirect off;
            proxy_buffering off;
            proxy_pass http://localhost:90;
         }
      }
