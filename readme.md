# Demo of transfer learning for object recognition using InceptionResNetV2

InceptionResNetV2 is a convolutional neural network that achieves new state-of-the-art in terms of accuracy for the ILSVRC image classification challenge.
This base model has been trained on the entirety of ImageNet which consists of about 14,000,000 images.
We have applied transfer learning to this state-of-the art model to recognize several objects in our lab (n = 6 classes).

InceptionResNetV2 combines the characteristics of the Inception and ResNet networks.
Instead of single convolutional layers it uses Inception modules that allow it do perform many different convolutional operations with different filter sizes as well as pooling and then just channel concatenate the results.
Similarly to ResNet, it's architecture that connects activations of previous layers to the deeper layers allow us to create deeper neural networks alleviating the vanishing gradient problem.  

![InceptionResNetV2](https://bytebucket.org/nordlinglab/nordlinglab-demotransferlearning/raw/ce44efc441922c3e94155e6496c8745eaac9e115/images/InceptionResnet.png)

Figures from Inception-v4, Inception-ResNet and the Impact of Residual Connections on Learning (Szegedy et al. 2017)

Typically, the procedure for transfer learning consists of:

1.  Load the model with pretrained weights.
1.  Remove the last layer of the model.
1.  Replace the last layer by a layer with the same number of classes as in our data.
1.  Freeze all the layers before the last layer.
1.  Retrain the parameters of the layer with our reduced dataset.

Transfer learning is specially useful when the data is scarce as the filters (feature extracting parts) of the network have been tuned through training on a large variety of images and have become invariant to small changes in the data.
This allows the technique to cut down training time while still achieving great results and to reduce the need for huge amounts of data.

![Transfer_learning_flowchart](https://bitbucket.org/nordlinglab/nordlinglab-demotransferlearning/raw/fe3208dd8569edacb70b67026c200e744f071990/Transfer_learning_flowchart.jpg)

This repository also contains the scripts for splitting videos into individual frames that serve as training data for our model and a script which uses the model to predict each frame of a live video feed.
It is important to note that InceptionResNetV2's previous-to-last layer has *1536* neurons and thus the number of parameters required to train is *n x 1536* where n is the number of classes we want to classify among.
A good dataset will consist of at least 1500 images for each class of object where the pictures show the object in different positions, backgrounds, lightings, distances from camera, and scenarios where the object might be found.

![Live_demo](https://bitbucket.org/nordlinglab/nordlinglab-demotransferlearning/raw/088a5804308ecca7c53055e36224fcb3505f8d73/Live_demo.jpg)

Check out our [YouTube video](https://youtu.be/Bu0WE4ljEyQ) of us using the neural network to do live object recognition on several objects from our lab.

This project is done by Bachelor student Yong-Kai "Kyle" Liu (劉永凱) and PhD student Jose Ramon Chang (張禾孟) at the Nordlinglab in the Mechanical Engineering department at NCKU.

Contact: jose.chang@nordlinglab.org
