import tensorflow as tf
import numpy as np
import cv2
import requests
import json
import os
import time
import colorsys
import torch

from numpy import random
from utils.general import non_max_suppression, scale_coords
from utils.datasets import letterbox
from utils.torch_utils import select_device
from base64 import b64encode
from io import BytesIO
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))

# Yolo v1m
conf_thresh = 0.05
IMAGE_SIZE = 448
GRID = 7
COLORS = [(215, 25, 28), (253, 174, 97), (171, 217, 233), (44, 123, 182)]
LABELS = ['Glasses', 'Scissor', 'Smartphone', 'Tape']

# Yolo v5
v5_IMAGE_SIZE = 1280
v5_conf_thres = 0.5
stride = 64
device = select_device('')

# Get names and colors
yolov5_names = [ 'person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
         'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
         'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee',
         'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard',
         'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
         'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch',
         'potted plant', 'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone',
         'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear',
         'hair drier', 'toothbrush' ]
v5colors = [[random.randint(0, 255) for _ in range(3)] for _ in yolov5_names]

# model = load_model(os.path.join(THIS_FOLDER, 'model/model.h5'), compile=False)
# weights = 'yolov5m6_saved_model'
# modelv5 = load_model(weights)

def preprocess_image(image_path, img_sz, model_id=None):
    img = cv2.resize(image_path, (img_sz, img_sz))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = img_to_array(img)
    if model_id == '5':
        img = np.reshape(img, (3, 1280, 1280))
    img = np.expand_dims(img, axis=0)/255
    return img

def get_prediction(image):

    data = json.dumps({
    "signature_name": "serving_default",
    'instances': image.tolist()
    })
    response = requests.post(f"http://serving:8501/v1/models/yolov1m_model:predict", data=data.encode('utf-8'))
    result = json.loads(response.text)
    prediction = np.squeeze(result['predictions'][0])

    return prediction

def get_prediction_v5(image):

    data = json.dumps({
        "signature_name": "serving_default",
        'instances': image.permute(0, 2, 3, 1).numpy().tolist()
    })
    response = requests.post(f"http://serving:8501/v1/models/yolov5_model:predict", data=data.encode('utf-8'))
    result = json.loads(response.text)
    prediction = result['predictions']
    prediction = np.asarray(prediction)

    return prediction

def output_predicted_image_v5(input_img):
    img_h, img_w, _ = input_img.shape

    # Preprocess image
    pre_img = letterbox(input_img, v5_IMAGE_SIZE, stride=stride)[0]
    pre_img = pre_img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB
    pre_img = np.ascontiguousarray(pre_img)

    # Run inference
    img = torch.zeros((1, 3, v5_IMAGE_SIZE, v5_IMAGE_SIZE))  # init img
    img = torch.from_numpy(pre_img)
    img = img.float()  # uint8 to fp16/32
    img /= 255.0  # 0 - 255 to 0.0 - 1.0
    if img.ndimension() == 3:
        img = img.unsqueeze(0)

    # Predict
    # pred = modelv5(img, training=False)
    pred = get_prediction_v5(img)

    # Denormalize xywh
    pred[..., :4] *= v5_IMAGE_SIZE
    pred = torch.tensor(pred)

    # Apply NMS
    pred = non_max_suppression(pred)

    thickness = int(0.01*img_w)
    font_thickness = int(0.002*img_w)
    font_scale = round(0.001*img_w, 1)
    if img_w > 1000 or img_h > 1000:
         thickness = round(0.002 * (img_w + img_h) / 2) + 1
         font_thickness = round(0.002 * (img_w + img_h) / 2) + 1
         font_scale = max(font_thickness - 7, 1)

    for i, det in enumerate(pred):  # detections per image
        if len(det):
            # Rescale boxes from img_size to im0 size
            det[:, :4] = scale_coords(img.shape[2:], det[:, :4], input_img.shape).round()
            det = det.numpy().tolist()
            # Write results
            for *xyxy, conf, cls in reversed(det):
                 # draw a bounding box rectangle and label on the image

                c1, c2 = (int(xyxy[0]), int(xyxy[1])), (int(xyxy[2]), int(xyxy[3]))
                input_img = cv2.rectangle(input_img, c1, c2, v5colors[int(cls)], thickness)
                text = f"{yolov5_names[int(cls)]}: {round(conf * 100, 2)}%"

                # calculate text width & height to draw the transparent boxes as background of the text
                (text_width, text_height) = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, font_scale, font_thickness)[0]
                text_offset_x = int(xyxy[0]) - 2
                text_offset_y = int(xyxy[1]) - 4
                box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + 2, text_offset_y - text_height))
                overlay = input_img.copy()
                overlay = cv2.rectangle(overlay, box_coords[0], box_coords[1], v5colors[int(cls)], cv2.FILLED)

                # add opacity (transparency to the box)
                input_img = cv2.addWeighted(overlay, 0.6, input_img, 0.4, 0)

                # now put the text (label: confidence %)
                input_img = cv2.putText(input_img, text, (text_offset_x, text_offset_y), cv2.FONT_HERSHEY_SIMPLEX, font_scale, (0, 0, 0), font_thickness)

    return input_img

def output_predicted_image(input_img):
    img_h, img_w, _ = input_img.shape
    image = preprocess_image(input_img, IMAGE_SIZE)
    pred = get_prediction(image)
    # pred = model.predict(image)
    # pred = pred[0]
    # Draw bounding box
    thickness = int(0.01*img_w)
    font_thickness = int(0.002*img_w)
    font_scale = round(0.001*img_w, 1)
    if img_w > 1000 or img_h > 1000:
         thickness = round(0.002 * (img_w + img_h) / 2) + 1
         font_thickness = round(0.002 * (img_w + img_h) / 2) + 1
         font_scale = max(font_thickness - 7, 1)
    result = np.zeros(len(LABELS), dtype=int)
    
    for by in range(GRID):
        for bx in range(GRID):
            bb = by * GRID + bx
            if pred[bb][0] > conf_thresh:

                # extract the bounding box coordinates
                x_start = int(img_w * (pred[bb][1] - 0.5 * pred[bb][3] + bx * 1/GRID))
                y_start = int(img_h* (pred[bb][2] - 0.5 * pred[bb][4] + by * 1/GRID))
                x_end = int(x_start + img_w * pred[bb][3])
                y_end = int(y_start + img_h * pred[bb][4])
                
                cls = np.argmax(pred[bb][5:])
                result[cls] = result[cls] + 1
                
                # draw a bounding box rectangle and label on the image
                input_img = cv2.rectangle(input_img, (x_start, y_start), (x_end, y_end), COLORS[cls], thickness)
                text = f"{LABELS[cls]}: {round(pred[bb][0] * 100, 2)}%"

                # calculate text width & height to draw the transparent boxes as background of the text
                (text_width, text_height) = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, font_scale, font_thickness)[0]
                text_offset_x = x_start - 2
                text_offset_y = y_start - 4
                box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + 2, text_offset_y - text_height))
                overlay = input_img.copy()
                overlay = cv2.rectangle(overlay, box_coords[0], box_coords[1], COLORS[cls], cv2.FILLED)

                # add opacity (transparency to the box)
                input_img = cv2.addWeighted(overlay, 0.6, input_img, 0.4, 0)

                # now put the text (label: confidence %)
                input_img = cv2.putText(input_img, text, (text_offset_x, text_offset_y), cv2.FONT_HERSHEY_SIMPLEX, font_scale, (0, 0, 0), font_thickness)
    
    return result, input_img

def encode_image(image):
    _, buffer = cv2.imencode(".jpg", image)
    buf = BytesIO(buffer)
    byte_im = b64encode(buf.getvalue()).decode('ascii')
    return byte_im

def save_image_frames(dummy_save_folder, image, no_frame):
    file_path = f"{dummy_save_folder}/{'%010d' % no_frame}.jpg"
    cv2.imwrite(file_path, image)

def create_prediction_video(dummy_save_folder):
    img_array = []
    files = os.listdir(dummy_save_folder)
    files = sorted(files)
    for filename in files:
        img = cv2.imread(os.path.join(dummy_save_folder, filename))
        img_array.append(img)
    height, width, _ = img.shape
    out = cv2.VideoWriter(dummy_save_folder + '/video.avi',cv2.VideoWriter_fourcc(*'DIVX'), 30, (width, height))
    for i in range(len(img_array)):
        out.write(img_array[i])
    out.release()
    os.system("ffmpeg -y -i " + dummy_save_folder + '/video.avi' + " -vcodec libx264 " + dummy_save_folder + '/video.mp4')
    os.remove(dummy_save_folder+'/video.avi')

def predict_video_frames(path_to_video, dummy_save_folder, model_id):
    # Load video
    cap = cv2.VideoCapture(path_to_video)
        
    # Process every frame in the video
    no_frame = 0

    if cap.isOpened(): 
        vid_w = int(cap.get(3))
        vid_h = int(cap.get(4))
    while(cap.isOpened()):
        ret, frame = cap.read()
        if not ret:
            break
        no_frame += 1
        if model_id == '1':
            _, predicted_image = output_predicted_image(frame)
        elif model_id == '5':
            predicted_image = output_predicted_image_v5(frame)
        save_image_frames(dummy_save_folder, predicted_image, no_frame)
    
    cap.release()
    cv2.destroyAllWindows()

    # Remove original video
    os.remove(path_to_video)

    return vid_w, vid_h