import os
import datetime
import time
import model
import numpy as np
import cv2
import tensorflow as tf

from urllib.parse import quote
from flask import Flask, flash, request, redirect, render_template
from werkzeug.utils import secure_filename 
from apscheduler.schedulers.background import BackgroundScheduler

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.experimental.set_visible_devices(gpus[0], 'GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)

# Initialization
THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
FOLDER_TO_REMOVE = []
UPLOAD_FOLDER = 'uploaded_video/'
IMG_EXT = ['png', 'jpg', 'jpeg', 'JPG', 'PNG', 'JPEG']
VIDEO_EXT = ['mp4', 'avi', 'mov', 'mpeg', 'flv', 'wmv', 'MP4', 'AVI', 'MOV', 'MPEG', 'FLV', 'WMV']
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', '.JPG', '.PNG', '.JPEG', 'mp4', 'avi', 'mov', 'mpeg', 'flv', 'wmv', 'MP4', 'AVI', 'MOV', 'MPEG', 'FLV', 'WMV'}

app = Flask(__name__)
scheduler = BackgroundScheduler()
scheduler.start()
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024
# Set the secret key to some random bytes. Keep this really secret!
app.secret_key = b'_5#sf21g8z\[n\xec]/'

predict_img_counts = 0
predict_video_counts = 0

def scheduledtask(*args):
    os.remove(args[0]+'/video.mp4')
    os.rmdir(args[0])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    global predict_img_counts
    global predict_video_counts
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        elif 'model_id' not in request.form:
            model_id = '5'
        elif 'model_id' in request.form:
            model_id = request.form['model_id']

        file = request.files['file']
        # if user does not select file, browser also submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            ext = filename.split('.')[-1]
            # print('Model id: ', model_id, type(model_id))
            if ext in IMG_EXT:
                # Process image
                img = cv2.imdecode(np.frombuffer(request.files['file'].read(), np.uint8), cv2.IMREAD_COLOR)
                img_h, img_w, _ = img.shape

                # Make prediction
                if model_id == '1':
                    result, img = model.output_predicted_image(img)
                    byte_im = model.encode_image(img)

                    # Pass predicted image in data uri
                    data_url = 'data:image/jpg;base64,{}'.format(quote(byte_im))

                    predict_img_counts += 1
                    # Render html for displaying the result
                    return render_template('prediction.html', data_url=data_url, IMAGE_W=img_w, IMAGE_H=img_h, result=result, \
                        predict_img_counts=predict_img_counts, predict_video_counts=predict_video_counts)
                elif model_id == '5':
                    img = model.output_predicted_image_v5(img)
                    byte_im = model.encode_image(img)

                    # Pass predicted image in data uri
                    data_url = 'data:image/jpg;base64,{}'.format(quote(byte_im))

                    predict_img_counts += 1
                    # Render html for displaying the result
                    return render_template('predictionv5.html', data_url=data_url, IMAGE_W=img_w, IMAGE_H=img_h, \
                        predict_img_counts=predict_img_counts, predict_video_counts=predict_video_counts)

            elif ext in VIDEO_EXT:
                time_name = time.strftime("%Y%m%d-%H%M%S")
                dummy_save_folder = os.path.join(THIS_FOLDER, 'static/dummy_images', time_name)
                path_to_video = os.path.join(THIS_FOLDER, 'static', app.config['UPLOAD_FOLDER'], filename)

                # Check if the directory exists, and make directory if dont exists
                if not os.path.exists(THIS_FOLDER + '/static/dummy_images'):
                    os.makedirs(THIS_FOLDER + '/static/dummy_images')
                if not os.path.exists(THIS_FOLDER + '/static/' + app.config['UPLOAD_FOLDER']):
                    os.makedirs(THIS_FOLDER + '/static/' + app.config['UPLOAD_FOLDER'])
                os.makedirs(dummy_save_folder)

                # Save video
                file.save(path_to_video)
                
                # Predict video frames
                vid_w, vid_h = model.predict_video_frames(path_to_video, dummy_save_folder, model_id)

                # Create video from processed frames, first create .avi then convert to .mp4
                model.create_prediction_video(dummy_save_folder)

                # Delete .jpg files in the folder
                files_in_directory = os.listdir(dummy_save_folder)
                for item in files_in_directory:
                    if item.endswith(".jpg"):
                        os.remove(os.path.join(dummy_save_folder, item))

                # Schedule to delete .mp4 video in 10 minutes
                FOLDER_TO_REMOVE.append(time_name)
                job_time = datetime.datetime.now() + datetime.timedelta(seconds=600)
                scheduler.add_job(func=scheduledtask, args=[dummy_save_folder], trigger='date', run_date=job_time, id=time_name)
                predict_video_counts += 1
                return render_template('prediction_video.html', IMAGE_W=vid_w, IMAGE_H=vid_h, video_path='dummy_images/'+time_name+'/video.mp4', \
                    predict_video_counts=predict_video_counts, predict_img_counts=predict_img_counts)
                
    # Render root html when there is no image uploaded
    return render_template('main.html', predict_video_counts=predict_video_counts, predict_img_counts=predict_img_counts)

# Service worker
@app.route('/sw.js')
def sw():
    return app.send_static_file('sw.js')
