// //pie
// var ctxP = document.getElementById("pieChart").getContext('2d');
// var myPieChart = new Chart(ctxP, {
//   type: 'doughnut',
//   data: {
//     labels: ["Glasses", "Scissor", "Phone", "Tape"],
//     datasets: [{
//       data: [1430, 1586, 1433, 1678],
//       backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
//       hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"],
//       hoverOffset: 4
//     }]
//   },
//   options: {
//     responsive: true,
//     legend: {
//       position: 'right'
//     },
//     plugins: {
//       datalabels: {
//         display: true,
//         backgroundColor: '#ccc',
//         borderRadius: 3,
//         font: {
//           color: 'red',
//           weight: 'bold',
//         },
//         formatter: (value, ctx) => {
                
//           let sum = 0;
//           let dataArr = ctxP.chart.data.datasets[0].data;
//           dataArr.map(data => {
//               sum += data;
//           });
//           let percentage = (value*100 / sum).toFixed(2)+"%";
//           return percentage;
//         }
//       },
//       doughnutlabel: {
//         labels: [{
//           text: '5500',
//           font: {
//             size: 20,
//             weight: 'bold'
//           }
//         }, {
//           text: 'total'
//         }]
//       }
//     }
//   }
// });

var data = [{
  data: [1430, 1586, 1433, 1678],
  backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
  hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
}];

var options = {
  responsive: true,
  legend: {
    position: 'right'
  },
  plugins: {
    datalabels: {
      formatter: (value, ctx) => {

        let sum = ctx.dataset._meta[0].total;
        let percentage = (value * 100 / sum).toFixed(2) + "%";
        return percentage;
      },
      color: '#fff',
    }

  }
};


var ctx = document.getElementById("pieChart").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'pie',
  data: {
    labels: ["Glasses", "Scissor", "Phone", "Tape"],
    datasets: data
  },
  options: options
});

