
const hiddenInput = document.getElementById('uploadinput');
const dropRegion = document.getElementById('drop-region');

// For upload button
dropRegion.addEventListener('click', function() {
    hiddenInput.click();
});

// Automatically submit the upload image
document.getElementById("uploadinput").onchange = function() {
    document.getElementById("upload-form").submit();
    document.getElementsByClassName("drop-message")[0].innerHTML = 'Processing...';
};

// For upload drag zone
// Detect if 'drag and drop' supported by the browser
var isAdvancedUpload = function() {
    var div = document.createElement('div');
    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
  }();
 
// change the message if 'drag and drop' is not supported
if (!isAdvancedUpload) {
    document.getElementsByClassName("drop-message")[0].innerHTML = 'Click to upload';
}  

// We can style the form by adding a class to it in the case of support

if (isAdvancedUpload) {
    dropRegion.addEventListener('dragenter', function(e) {
        e.preventDefault();
        e.stopPropagation();
        dropRegion.classList.add("dragover");
    });

    dropRegion.addEventListener('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
        dropRegion.classList.add("dragover");
    });

    dropRegion.addEventListener('dragleave', function(e) {
        e.preventDefault();
        e.stopPropagation();
        dropRegion.classList.remove("dragover");
    });

    dropRegion.addEventListener('dragend', function(e) {
        e.preventDefault();
        e.stopPropagation();
        dropRegion.classList.remove("dragover");
    });

    dropRegion.addEventListener('drop', function(e) {
        e.preventDefault();
        e.stopPropagation();
        dropRegion.classList.remove("dragover");
        uploadinput.files = e.dataTransfer.files;
        document.getElementById("upload-form").submit();
        document.getElementsByClassName("drop-message")[0].innerHTML = 'Processing...';
    });
  
}
