The webpage is [http://where.nordlinglab.org/](http://where.nordlinglab.org/)

# How to update the page content: #
1. Make sure the latest HTML is already good to go in development server.
1. Make sure server is on (Canada server, IP:140.116.155.14).
1. Connect with the server from another computer using a terminal. To connect, you have to be a user, and with your username, write in the terminal:

        ssh username@140.116.155.14 -p60002

1. Being in the server, run the code below
        
        cd /home/gavin/Nordlinglab-ObjectDetection/
        git pull
        sudo docker-compose up --build -d

# How to reupload the page: #
1. Make sure server is on (Canada server, IP:140.116.155.14).
1. Connect with the server from another computer using a terminal. To connect, you have to be a user, and with your username, write in the terminal:

        ssh username@140.116.155.14 -p60002

1. Being in the server, run the code below
        
        cd /home/gavin/Nordlinglab-ObjectDetection/
        sudo docker-compose up -d

__If you want to update the nginx configuration for external connection:__

        sudo nano /etc/nginx/conf.d/example.conf

Be aware that this file is the same one for ProtFunAI nginx configuration.

